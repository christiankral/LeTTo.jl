module Letto

    using ElectricalEngineering, PyPlot
    export letto_winding_mmf_plot

    function letto_winding_mmf_plot(w, wfake, i; r = 0.2,
        color = ["#E6550D", "#8CA252", "#6B6ECF", "black"],
        slot = 1, yoke = 1, fillcolor = "#D9D9D9",
        showsum = true, label = ["1", "2", "3", L"$\Sigma$"],
        linestyle=[lineStyle1,lineStyle3,lineStyle2,lineStyle4],
        linewidth=[lineWidth1,lineWidth3,lineWidth2,lineWidth4],
        showlegend = true, loc = "best",
        index = collect(1:size(winding_mmf(w,i),1)),
        showslot = true, start = 1, inc = 1, showmmf = true,
        mmf1_label_neg = L"$-\hat V_{m,1}$", mmf1_label_pos = L"$+\hat V_{m,1}$",
        mmf_label_neg = L"$-\hat V_{m}$", mmf_label_pos = L"$+\hat V_{m}$")

        # Plot winding layout
        subplot2grid((3, 1), (0, 0))
        winding_plot(w, r = 0.2,
            color = color,
            slot = slot, yoke = yoke, fillcolor = fillcolor)
        # Determine exact extension of plot area
        ax = gca()

        # Plot MMF
        subplot2grid((3, 1), (1, 0), rowspan = 2)
        mmf = winding_mmf(wfake,i)
        (mmf1_max, mmf_max) = mmf_plot(mmf, index = index,
            color = color,
            showsum = showsum, label = label,
            linestyle = linestyle,
            linewidth = linewidth,
            showlegend = showlegend, loc = loc)
        if showslot
            slot_label(size(mmf,2), start = start, inc = inc)
        end
        mmf_label(mmf1_max, mmf_max = mmf_max, showmmf = showmmf,
            mmf1_label_neg = mmf1_label_neg, mmf1_label_pos = mmf1_label_pos,
            mmf_label_neg = mmf_label_neg, mmf_label_pos = mmf_label_pos)
        # Re-adjust by doing everything twice
        tight_layout()
        xlim(ax.axis()[1],ax.axis()[2])
        tight_layout()
        xlim(ax.axis()[1],ax.axis()[2])
    end
end
